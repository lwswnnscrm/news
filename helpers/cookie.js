/*eslint-disable */

const cookie = (cook, name) => {
  const cookieString = `; 1st element arr; ${cook}`;
  const result = cookieString.split(`; ${name}=`);
  return result.length === 2 ? result.pop().split(';').shift() : null;
};

const deleteCookie = (name) => {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

export default cookie;
