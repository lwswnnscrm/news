const host = 'http://localhost:8080/api/v1';

const api = {
  products: `${host}/products`,
  product: `${host}/product/:id`,
  createProduct: `${host}/product`,
  categories: `${host}/categories`,
  login: `${host}/login`,
  register: `${host}/register`,
  verifyToken: `${host}/auth-token`,
};

export default api;
