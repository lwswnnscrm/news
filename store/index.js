/*eslint-disable */
import axios from 'axios';
import api from '~/constants/api';
import cookie from '~/helpers/cookie'

export const state = () => ({
  c_t: null,
});

export const mutations = {
  SET_TOKEN(state, data) {
    state.c_t = data;
  },
};

export const actions = {
  async nuxtServerInit ({ commit }, { req }) {
    // commit('SET_TOKEN', req.headers.cookie);
    if (req.headers.cookie) {
      const token = await cookie(req.headers.cookie, 'c_t');
      if (token) {
        const { data } = await axios.post(api.verifyToken, { token });
        commit('user/SET_USER', data[0]);
        commit('SET_TOKEN', token);
      }
    }
  },
};
