import axios from 'axios';
import api from '~/constants/api';

export const state = () => ({
  products: [],
  product: null,
  error: null,
});

export const mutations = {
  SET_PRODUCTS(state, products) {
    state.products = products;
  },
  SET_PRODUCT(state, product) {
    state.product = product;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetch({ commit }, params) {
    const { data } = await axios.get(api.products, { params });
    commit('SET_PRODUCTS', data);
  },

  async fetchItem({ commit }, id) {
    const { data } = await axios.get(api.product.replace(':id', id));
    commit('SET_PRODUCT', data);
  },

  createItem({ commit }, item) {
    axios.post(api.createProduct, item)
      .then((resp) => {
        console.log(resp);
      })
      .catch((e) => {
        commit('SET_ERROR', e);
        console.log(e);
      });
  },
};
