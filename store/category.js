import axios from 'axios';
import api from '~/constants/api';

export const state = () => ({
  categories: [],
});

export const mutations = {
  SET_CATEGORIES(state, categories) {
    state.categories = categories;
  },
};

export const actions = {
  async fetch({ commit }) {
    const { data } = await axios.get(api.categories);
    commit('SET_CATEGORIES', data);
  },
};
