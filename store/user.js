import axios from 'axios';
import api from '~/constants/api';

export const state = () => ({
  user: null,
});

export const mutations = {
  SET_USER(state, userObj) {
    state.user = userObj;
  },
};

export const actions = {
  async fetch({ commit }, { user, type }) {
    try {
      const { data } = await axios.post(type === 'login' ? api.login : api.register, user);
      localStorage.setItem('c_t', data[0].token);
      document.cookie = `c_t= ${data[0].token}`;
      commit('SET_USER', data[0]);
    } catch (error) {
      console.log(error.response.data.message);
    }
  },

  signOut({ commit }) {
    commit('SET_USER', null);
    localStorage.removeItem('c_t');
  },
};
