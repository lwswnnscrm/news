export default function ({ store, redirect }) {
  if (!store.state.c_t && !store.state.user.user) {
    return redirect('/admin/login');
  }
  return redirect();
}
