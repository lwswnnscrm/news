module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'airbnb-base',
    'plugin:vue/recommended'
  ],
  // add your custom rules here
  rules: {
    'no-param-reassign': ['error', { 'props': true, 'ignorePropertyModificationsFor': ['state', 'getters', 'context'] }],
    'no-shadow': ['error', { 'allow': ['state', 'getters'] }],
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 'off',
  }
}
